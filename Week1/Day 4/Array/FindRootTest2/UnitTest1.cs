using FindRoot2;

namespace FindRootTest2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrang
            int input = 27;
            int expected = 3;

            //Act
            int actual = Root.FindRoot(input);

            //Asert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod2()
        {
            //Arrang
            int input = 28;
            int expected = 0;

            //Act
            int actual = Root.FindRoot(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}