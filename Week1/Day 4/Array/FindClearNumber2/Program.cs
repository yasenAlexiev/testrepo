﻿namespace FindClearNumber2 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 2, 3, 4, 5, 7};

            double findNumb = FindMissingNumber(arr);
            Console.WriteLine(findNumb);
        }

        public static double FindMissingNumber(int[] arr)
        {
            // Check Array length
            if (arr.Length == 2)
            {
                throw new ArgumentException("Array is empty");
            }

            int minNumber = arr[0];
            int maxNumber = 0;
            int arrLength = arr.Length + 1;
            int sumArray = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (maxNumber < arr[i]) 
                {
                    maxNumber = arr[i];
                }

                if (minNumber > arr[i]) 
                { 
                    minNumber = arr[i]; 
                }

                sumArray += arr[i];
            }

            int number = ((minNumber + maxNumber) * arrLength) / 2 ;

            int findNumber = number - sumArray;

            return findNumber;
        }
    }
}