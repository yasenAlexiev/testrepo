﻿namespace FindRoot2 // Note: actual namespace depends on the project name.
{
    public class Root
    {
        static void Main(string[] args)
        {

            int input = int.Parse(Console.ReadLine());

            int root = FindRoot(input);

            Console.WriteLine(root);
        }

        public static int FindRoot(int input)
        {
            int currentRoot = 0;
            // Must be optimised

            while (true)
            {
                int root = currentRoot * currentRoot * currentRoot;

                if (root == input || root > input)
                {
                    return currentRoot;
                }

                currentRoot++;
            }

            
        }
    }
}