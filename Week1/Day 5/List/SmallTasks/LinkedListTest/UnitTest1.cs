using LinkedList;

namespace LinkedListTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestRemoveElementFromListWithOddNUmber()
        {
            //Arrange
            LinkedList<int> input = new LinkedList<int>();
            input.AddLast(1);
            input.AddLast(2);
            input.AddLast(3);
            input.AddLast(4);
            input.AddLast(5);
            input.AddLast(6);
            input.AddLast(7);

            LinkedList<int> expected = new LinkedList<int>();
            expected.AddLast(1);
            expected.AddLast(2);
            expected.AddLast(3);
            expected.AddLast(5);
            expected.AddLast(6);
            expected.AddLast(7);

            //Act
            var actual = Program.RemoveElement(input);

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestRemoveElementFromListWithEvenNUmber()
        {
            //Arrange
            LinkedList<int> input = new LinkedList<int>();
            input.AddLast(1);
            input.AddLast(2);
            input.AddLast(3);
            input.AddLast(4);
            input.AddLast(5);
            input.AddLast(6);

            LinkedList<int> expected = new LinkedList<int>();
            expected.AddLast(1);
            expected.AddLast(2);
            expected.AddLast(4);
            expected.AddLast(5);
            expected.AddLast(6);

            //Act
            var actual = Program.RemoveElement(input);

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}