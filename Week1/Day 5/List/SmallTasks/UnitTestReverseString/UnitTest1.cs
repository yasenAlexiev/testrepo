using SmallTasks;

namespace UnitTestReverseString
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestReversString()
        {
            //Arrange
            string input = "Hello";
            string expected = "olleH";
            //Act

            string actual = ReverseString.Reverse(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}