using ReturnUniqueElements;

namespace ResturnUniqueElementsTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestUniqueList()
        {
            //Arrange
            List<int> input = new List<int>() { 1, 2, 3, 4, 3, 5, 2, 3, 5 };
            List<int> expected = new List<int>() { 1, 2, 3, 4, 5 };
            //Act
            var actual = Program.FindUniqNumber(input);

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUniqueList1()
        {
            //Arrange
            List<int> input = new List<int>() { 1, 1, 1, 1, 1, 1, 2, 3, 5 };
            List<int> expected = new List<int>() { 1, 2, 3, 5 };
            //Act
            var actual = Program.FindUniqNumber(input);

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}