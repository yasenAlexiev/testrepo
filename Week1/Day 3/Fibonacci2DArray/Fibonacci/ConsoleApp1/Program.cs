﻿using System;

namespace ConsoleApp1
{
    public class Program
    {


        static bool isPerfectSquare(int x)
        {
            int s = (int)Math.Sqrt(x);
            return (s * s == x);
        }
        static bool isFibonacciMethod(int n)
        {
            return isPerfectSquare(5 * n * n + 4) ||
                   isPerfectSquare(5 * n * n - 4);
        }


        static bool IsFibonacci(int n)
        {
            // A number is a Fibonacci number if and only if 5*n*n+4 or 5*n*n-4 is a perfect square
            int x = 5 * n * n + 4;
            int y = 5 * n * n - 4;
            int sqrtX = (int)Math.Sqrt(x);
            int sqrtY = (int)Math.Sqrt(y);
            return sqrtX * sqrtX == x || sqrtY * sqrtY == y;
        }


        //Find if there is a square with fibonacci numbers (not consecutive)
        public static bool AreAllFibonacci(int[,] arr)
        {
            

            int rows = arr.GetLength(0);
            int cols = arr.GetLength(1);
            for (int i = 0; i <= rows - 2; i++)
            {
                for (int j = 0; j <= cols - 2; j++)
                {
                    bool isFibonacci = true;

                    //List<int> result = new List<int>();

                    for (int k = i; k <= i + 1; k++)
                    {
                        for (int l = j; l <= j + 1; l++)
                        {

                            //result.Add(arr[k, l]);

                            //Console.WriteLine(result);
                            if (!isFibonacciMethod(arr[k, l]))
                            {
                                isFibonacci = false;
                                break;
                            }

                        }
                        if (!isFibonacci) break;
                    }


                    if (isFibonacci) return true;
                }
            }
            return false;
        }


        static void Main(string[] args)
        {       
        }
    }
}