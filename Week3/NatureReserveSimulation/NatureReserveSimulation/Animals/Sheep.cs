﻿using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Sheep : Animal
    {
        private const string Name = "Sheep";
        private const int Energy = 4;

        static List<Food> Diet = new List<Food>()
        {
            new Fruits(),
            new Grass(),
        };

        public Sheep()
            : base(Name, Energy, Diet, 7, false)
        {
        }

        public override void GetDiet()
        {
            var newDiet = new List<Food>
            {
                new Vegetable()
            };
            Diet.AddRange(newDiet);
            
        }

        public override string Starving()
        {
            return $"{base.Name} starving";
        }
    }
}
