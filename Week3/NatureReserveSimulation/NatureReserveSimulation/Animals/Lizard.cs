﻿using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Lizard : Animal
    {
        private const string Name = "Lizard";
        private const int Energy = 1;

        static List<Food> Diet = new List<Food>()
        {
            new Vegetable()
        };

        public Lizard()
            : base(Name, Energy, Diet, 5, false)
        {
        }

        public override void GetDiet()
        {
            var newDiet = new List<Food>
            {
                new Grass(),
                new Insect(),
                new Egg()
            };
            Diet.AddRange(newDiet);
        }

        public override string Starving()
        {
            return $"{base.Name} not eat";
        }
    }
}
