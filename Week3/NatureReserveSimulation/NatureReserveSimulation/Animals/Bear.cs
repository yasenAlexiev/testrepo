﻿using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Bear : Animal
    {
        private const string Name = "Bear";
        private const int Energy = 5;


        static List<Food> Diet = new List<Food>()
        {
            new Fish(),
        };

        public Bear()
            : base(Name, Energy, Diet, 1, false)
        {
        }

        public override void GetDiet()
        {
            var newDiet = new List<Food>
            {
                new Honey(),
                new Sheep(),
                new Lizard(),
                new Egg()
            };
            Diet.AddRange(newDiet);
        }

        public override string Starving()
        {
            return $"Hungry {base.Name} sounds";
        }
    }
}
