﻿using NatureReserveSimulation.Foods;
using System.Text;

namespace NatureReserveSimulation.Animals
{

    public abstract class Animal : Food
    {
        public int MaxEnergy { get; set; }
        public int CurrentEnergy { get; set; }
        protected List<Food> Diet { get; set; }
        public int Lifespan { get; set; }

        public int ReadyToMature { get; set; }

        protected bool Regenerate { get; set; }

        protected Animal(string name, int maxEnergy, List<Food> diet, int readyToMature, bool regenerate)
            : base(maxEnergy)
        {
            MaxEnergy = maxEnergy;
            CurrentEnergy = maxEnergy;
            Diet = diet;
            Lifespan = 0;
            ReadyToMature = readyToMature;
            Regenerate = false;
        }
        

        public void Feed(Food currentFood, StringBuilder details)
        {
            if (IsActive())
            {
                int missingEnergy = MaxEnergy - CurrentEnergy;

                if (Diet.Contains(currentFood)
                    && currentFood.NutritionalValue > missingEnergy
                    && missingEnergy > 0
                    && CurrentEnergy < MaxEnergy)
                {
                    details.AppendLine(Eating(missingEnergy, currentFood));
                }
                else
                {
                    CurrentEnergy--;
                    details.AppendLine(Starving());
                    
                    if (!IsActive())
                    {
                        details.AppendLine($"{this.Name} Dead ");
                    }
                }

                Lifespan++;

                if (Lifespan == ReadyToMature)
                {
                    GetDiet();
                }
            }
            else
            {
                details.AppendLine($"{this}");
            }
            
        }

        public string Eating(int missingEnergy, Food currentFood)
        {
            
            CurrentEnergy += missingEnergy;
            currentFood.NutritionalValue -= missingEnergy;

            return $"{this.Name} eat {currentFood.Name} and energy is {CurrentEnergy}";
        }

        public abstract string Starving();
        public abstract void GetDiet();
        
        public virtual bool IsActive()
        {
            return CurrentEnergy > 0 && NutritionalValue == MaxNutritionValue;
        }
    }
}
