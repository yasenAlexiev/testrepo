﻿using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Snake : Animal
    {
        private const string Name = "Snake";
        private const int Energy = 5;

        static List<Food> Diet = new List<Food>()
        {
            new Egg()
        };

        public Snake()
            : base(Name, Energy, Diet, 8, false)
        {
        }

        public override void GetDiet()
        {
            var newDiet = new List<Food>
            {
                new Insect(),
                new Lizard()
            };
            Diet.AddRange(newDiet);
        }

        public override string Starving()
        {
            return "Snake is sleep and not hungry";
        }
    }
}
