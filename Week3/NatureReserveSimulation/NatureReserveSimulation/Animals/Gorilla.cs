﻿using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Gorilla : Animal
    {
        private const string Name = "Gorilla";
        private const int Energy = 6;

        static List<Food> Diet = new List<Food>()
        {
            new Fruits()
        };

        // old diet

        public Gorilla()
            : base(Name, Energy, Diet, 6, false)
        {
        }

        public override void GetDiet()
        {
            var newDiet = new List<Food>
            {
                new Grass(),
                new Insect()
            };
            Diet.AddRange(newDiet);
        }

        public override string Starving()
        {
            return $"{base.Name} not hungry";
        }
    }
}
