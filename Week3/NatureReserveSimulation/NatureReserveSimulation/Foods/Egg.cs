﻿namespace NatureReserveSimulation.Foods
{
    public class Egg : Food
    {
        private const int defaultNutrioneValue = 4;

        public Egg()
            : base(defaultNutrioneValue)
        {
        }

    }
}
