﻿namespace NatureReserveSimulation.Foods
{
    public class Grass : Food
    {
        private const int defaultNutrioneValue = 3;

        public Grass()
            : base(defaultNutrioneValue)
        {
            isPlant = true;
        }
    }
}
