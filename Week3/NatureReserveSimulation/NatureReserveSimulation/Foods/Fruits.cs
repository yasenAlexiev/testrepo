﻿namespace NatureReserveSimulation.Foods
{
    public class Fruits : Food
    {
        private const int defaultNutrioneValue = 2;

        public Fruits()
            : base( defaultNutrioneValue)
        {
            isPlant = true;
        }

    }
}
