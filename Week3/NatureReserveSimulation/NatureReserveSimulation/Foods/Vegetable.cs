﻿namespace NatureReserveSimulation.Foods
{
    public class Vegetable : Food
    {
        private const int defaultNutrioneValue = 3;

        public Vegetable()
            : base(defaultNutrioneValue)
        {
            isPlant = true;
        }
    }
}
