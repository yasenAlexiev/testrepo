﻿namespace NatureReserveSimulation.Foods
{
    public class Insect : Food
    {

        private const int defaultNutrioneValue = 2;

        public Insect()
            : base(defaultNutrioneValue)
        {
        }
    }
}
