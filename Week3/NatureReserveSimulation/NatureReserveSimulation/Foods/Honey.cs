﻿namespace NatureReserveSimulation.Foods
{
    public class Honey : Food
    {
        private const int defaultNutrioneValue = 2;

        public Honey()
            : base(defaultNutrioneValue)  
        {
        }
    }
}
