﻿namespace NatureReserveSimulation.Foods
{
    public abstract class Food
    {
        public string Name { get; set; }

        public int NutritionalValue { get; set; }

        public int MaxNutritionValue { get; set; }

        protected bool isPlant;

        protected Food(int nutritionValue)
        {
            this.Name = GetType().Name;
            this.NutritionalValue = nutritionValue;
            this.MaxNutritionValue = nutritionValue;
        }

        public virtual void RegenerateFood(List<Food> foods)
        {

            foreach (var food in foods)
            {
                if (food.isPlant && food.NutritionalValue < food.MaxNutritionValue)
                {
                    food.NutritionalValue++;
                    Console.WriteLine($"Regenerate this {food.Name}");
                }

            }

        }


        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if (!(obj is Food)) return false;
            Food food = (Food)obj;

            return Name.Equals(food.Name);
        }
    }
}
