﻿using NatureReserveSimulation.Statistics;

namespace NatureReserveSimulation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string statisticDetail = "Detail";
            //string statisticInfo = "Summary";

            Statistic statistic = new Statistic();
            // Create a simulation with these parameters
            var simulation = new Simulator(statistic);

            // Run the simulation
            simulation.Run(statisticDetail);

            Console.WriteLine();
        }
    }
}