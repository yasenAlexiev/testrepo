﻿using NatureReserveSimulation.Animals;
using NatureReserveSimulation.Foods;
using NatureReserveSimulation.Statistics;
using System.Text;

namespace NatureReserveSimulation
{
    public class Simulator
    {
        private readonly Statistic _statistics;

        List<Animal> Animals = Data.Animals;
        List<Food> Foods = Data.Foods;

        public Simulator(Statistic statistics)
        {
            _statistics = statistics;
        }

        private Food RandomFood(List<Food> foods)
        {
            int randomFood = new Random().Next(foods.Count);
            return foods.ElementAt(randomFood);
        }


        public void Simulate(List<Animal> animals, List<Food> foods, string statisticInfo)
        {

            int count = 0;

            while (animals.Any(x => x.IsActive()))
            {
                Food randomFoot = RandomFood(foods);

                StringBuilder details = new StringBuilder();

                foreach (var animal in animals.Where(x => x.IsActive()))
                {
                    
                    count++;
                    animal.Feed(randomFoot, details);

                    if (statisticInfo == "Summary")
                    {
                        _statistics.Summary(count, animals);
                    }
                    else if (statisticInfo == "Detail")
                    {
                        _statistics.Details(details.ToString());
                    }
                    
                }
                randomFoot.RegenerateFood(foods);
            }

            int minLifespan = animals.Min(x => x.Lifespan);
            int maxLifespan = animals.Max(x => x.Lifespan);
            double avgLifespan = Math.Round(animals.Average(x => x.Lifespan));

            _statistics.Finally(minLifespan, maxLifespan, avgLifespan);
        }

        public void Run(string statisticInfo)
        {

            List<Animal> animals = Animals;
            List<Food> foods = Foods;
            

            Simulate(animals, foods, statisticInfo);
            
        }
    }
}
