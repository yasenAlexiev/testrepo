using Task2;

namespace Task2Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestWithRepeatElement()
        {

            int[] arr1 = { 1, 2, 3, 4, 8 };
            int[] arr2 = { 2, 3, 4, 5, 6, 7, 8 };

            List<int> result = Program.FindRepeatEl(arr1, arr2);
            List<int> expect = new List<int>() { 2, 3, 4, 8};

            CollectionAssert.AreEqual(result, expect);


        }

        public void TestWithNonRepeatElement()
        {

            int[] arr1 = { 9, 10, 11, 12 };
            int[] arr2 = { 2, 3, 4, 5, 6, 7, 8 };

            List<int> result = Program.FindRepeatEl(arr1, arr2);


            Assert.ThrowsException<ArgumentException>(() => result);


        }
    }
}