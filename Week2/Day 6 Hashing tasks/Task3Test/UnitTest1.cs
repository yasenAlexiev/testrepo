using Task3;

namespace Task3Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestReturnAllNonRepeatingCharacters()
        {

            string input = "test123tklesr0est";

            string result = Program.FindAllNonRepeatCharacters(input);
            string expect = "tes123klr0";

            Assert.AreEqual(expect, result);
        }


        [TestMethod]
        public void TestFindFirstNonRepeatCharacter()
        {

            string input = "texst523tklesr0est";

            char result = Program.FirstNonRepeatCharacter(input);
            char expect = 'x';

            Assert.AreEqual(expect, result);
        }
    }
}