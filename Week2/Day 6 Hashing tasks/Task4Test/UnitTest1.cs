using Task4;

namespace Task4Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestContainsAllMisspelledWords()
        {

            HashSet<string> set = new HashSet<string>();
            set.Add("Lorem");
            set.Add("Ipsum");
            set.Add("is");
            set.Add("simply");
            set.Add("dummy");
            set.Add("text");

            string input = "Lorem ipsum is simplly dummyy text.";

            HashSet<string> result = Program.ReturnsAllMisspelledWords(set, input);

            Assert.IsTrue(result.Contains("Ipsum"));
            Assert.IsTrue(result.Contains("simply"));
        }
    }
}