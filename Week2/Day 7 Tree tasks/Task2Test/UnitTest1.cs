using Task2;

namespace Task2Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestHeightTree()
        {
            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root = new BinaryTree(25, node11, node52);

            int heightTree = Program.HeightTree(root);
            int expect = 3;

            Assert.AreEqual(expect, heightTree);
        }

        [TestMethod]
        public void TestHeightTree2()
        {
            //var node76 = new BinaryTree(76, null, null);
            //var node61 = new BinaryTree(61, null, null);
            //var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, null, null);
            var node38 = new BinaryTree(38, null, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root = new BinaryTree(25, node11, node52);

            int heightTree = Program.HeightTree(root);
            int expect = 2;

            Assert.AreEqual(expect, heightTree);
        }
    }
}