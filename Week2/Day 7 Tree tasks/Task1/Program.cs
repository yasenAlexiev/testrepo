﻿
namespace Task1 
{
    public class Program
    {
        static void Main(string[] args)
        {

            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root =  new BinaryTree(25, node11, node52);

            

            List<int> result = new List<int>();
            List<int> inOrder = InOrder(root, result);
            //List<int> postOrder = PostOrder(root, result);
            //List<int> preOrder = PreOrder(root, result);

            foreach (var item in inOrder)
            {
                Console.WriteLine(item);
            }
        }

        public static List<int> PreOrder(BinaryTree root, List<int> result)
        {
            
            if (root != null)
            {
                result.Add(root.Value);
                PreOrder(root.Left, result);
                PreOrder(root.Right, result);
            }

            return result;
        }

        public static List<int> PostOrder(BinaryTree root, List<int> result)
        {

            if (root != null)
            {
                
                PostOrder(root.Left, result);
                PostOrder(root.Right, result);
                result.Add(root.Value);
            }

            return result;
        }

        public static List<int> InOrder(BinaryTree root, List<int> result)
        {

            if (root != null)
            {
                InOrder(root.Left, result);
                result.Add(root.Value);
                InOrder(root.Right, result);
            }

            return result;
        }
    }
}