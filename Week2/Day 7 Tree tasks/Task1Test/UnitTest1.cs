using Task1;

namespace Task1Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestInOrderReturnCorrectOutput()
        {
            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root = new BinaryTree(25, node11, node52);


            List<int> result = new List<int>();
            List<int> inOrder = Program.InOrder(root, result);

            List<int> resultTest = new List<int>() { 11, 24, 25, 35, 38, 52, 61, 69, 76 };

            CollectionAssert.AreEqual(resultTest, inOrder);
        }


        [TestMethod]
        public void TestPreOrderReturnCorrectOutput()
        {
            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root = new BinaryTree(25, node11, node52);


            List<int> result = new List<int>();
            List<int> inOrder = Program.PreOrder(root, result);

            List<int> resultTest = new List<int>() { 25, 11, 24, 52, 38, 35, 69, 61, 76 };

            CollectionAssert.AreEqual(inOrder, resultTest);
        }

        [TestMethod]
        public void TestPostOrderReturnCorrectOutput()
        {
            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root = new BinaryTree(25, node11, node52);


            List<int> result = new List<int>();
            List<int> inOrder = Program.PostOrder(root, result);

            List<int> resultTest = new List<int>() { 24, 11, 35, 38, 61, 76, 69, 52, 25 };

            CollectionAssert.AreEqual(inOrder, resultTest);
        }
    }
}